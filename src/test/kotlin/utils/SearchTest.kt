package utils

import kotlin.test.Test
import kotlin.test.assertEquals

internal class SearchTest {

    @Test
    fun testBinarySearch() {
        val array = intArrayOf(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51)

        // Тестируем, что метод возвращает корректный индекс для существующего элемента
        var targetValue = 5
        assertEquals(4, Search.binary(array, targetValue))
        targetValue = 15
        assertEquals(14, Search.binary(array, targetValue))

        // Тестируем, что метод возвращает -1 для отсутствующего элемента
        targetValue = 52
        assertEquals(-1, Search.binary(array, targetValue))
        targetValue = 100
        assertEquals(-1, Search.binary(array, targetValue))

        // Тестируем, что метод возвращает -1 для пустого массива
        targetValue = 10
        assertEquals(-1, Search.binary(intArrayOf(), targetValue))
    }
}