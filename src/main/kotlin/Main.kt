package ru.mirea

import utils.Search
import kotlin.random.Random

fun main(args: Array<String>) {
    val envValue = System.getenv("TARGET_VALUE")?.toIntOrNull()
    val argValue = args.getOrNull(0)?.toIntOrNull()

    println("ENV: $envValue\nargValue: $argValue")
    val targetValue =
        envValue ?:
        argValue ?:
        run {
            println("Начало: введите сумму цифр в дате рождения, умноженную на 2 (от 8 до 142)")
            return
        }

    val random = Random.Default
    val arraySize = 100
    val sortedArray = generateSortedArray(random, arraySize)

    println("Generated sorted array: ${sortedArray.joinToString()}")

    val foundIndex = Search.binary(sortedArray, targetValue)
    if (foundIndex!= -1) {
        println("Found number at index: $foundIndex")
    } else {
        println("Not found")
    }
}

fun generateSortedArray(random: Random, size: Int): IntArray {
    val array = IntArray(size)
    for (i in 0..<size) {
        array[i] = random.nextInt(1000)
    }
    array.sort()
    return array
}